# ChangeLog

## 0.4.1 - 2024-12-17

* Fixes the case when the virtual machine guest is registered under a
  different name, other than the name of the project folder on
  `__kvmx_initialize`.

* Fixes VM name and folder logic handling on `kvmx_init`.

## 0.4.0 - 2024-11-22

* Adds `kvmx sshdir` action, which SSH's to the guest and changes to a given
  folder. If no arguments are given, it tries to change to the equivalent
  folder in the guest, so it can be a way to `cd` to the same directory but
  inside the guest.

  If the folder in the host is also mounted in the guest in a similar
  mountpoint, it's a handy way to move to the same folder, but inside the
  guest.

  For folders inside $`HOME`, user name conversion is automatically done since
  the user inside the guest might not match the user in the host.

  Note: this is done in a best-effort basis. It might not work depending
  on the remote shell in use. To increase the chances, it's suggested that
  the remote shell's startup scripts support the `STARTUP_FOLDER` as
  follows.

  Example 1 (sh-compatible):

    # Only change to the startup folder if it's set
    if [ ! -z "$STARTUP_FOLDER" ]; then
      cd $STARTUP_FOLDER
    fi

  Example 2 (sh-compatible):

    # Make sure we start at the startup folder, defaulting to home
    cd $STARTUP_FOLDER

## 0.3.0 - 2024-09-19

* Increase the maximum number of shared folders to avoid error in when KVMX
  attempts to setup too many 9p folder shares:

        kvm: -drive file=/var/cache/qemu/$guest/box.img,if=virtio,discard=unmap:
        PCI: no slot/function available for virtio-blk-pci, all in use or reserved`

* Per-mountpoint mode option (ro, rw).

* Spice:
  * Improved spice window handling.

  * Inotify action for watching a folder in the host and issuing commands in the
    guest upon change.

  * Notification on Awesome VM when a Spice session taking too long to open.

## 0.2.0 - 2024-07-02

* Increase default `msize` for 9p mounts to 32MiB depending on kernel support,
  which may only be effective depending on `trans_virtio` driver support for
  sizes larger than 500kB.

* QEMU parameter updates:
  * Update `nowait` to `wait=off`.

  * Update `server` to `server=on`.

  * Update `unix` to `unix=on`.

  * Update `disable-ticketing` to `disable-ticketing=on`.

* Provide default parameters by loading the sample config.
  This allows a `kvmxfile` to be small and have only the overrides.

## 0.1.0 - 2024-05-17

* This software started on 2017-03-09, but only now it's getting an initial
  first release.
