# KVMX Documentation

Misc documentation on kvmx and QEMU.

## USB hotplug support

* Good to have `usbutils` package in the guest.

* Docs:
  * https://qemu.readthedocs.io/en/latest/system/usb.html
  * https://github.com/qemu/qemu/blob/master/docs/usb2.txt
  * https://wiki.gentoo.org/wiki/QEMU/Windows_guest
  * https://www.linux-kvm.org/page/USB_Host_Device_Assigned_to_Guest
  * https://en.wikibooks.org/wiki/QEMU/Devices/USB/Root
  * https://wiki.archlinux.org/index.php/QEMU#Pass-through_host_USB_device

* Issues:
  * https://serverfault.com/questions/666210/attaching-usb-dongle-to-kvm-vm
  * https://unix.stackexchange.com/questions/119335/how-can-i-simulate-usb-storage-device-connection-with-qemu#119530
  * https://unix.stackexchange.com/questions/452934/can-i-pass-through-a-usb-port-via-qemu-command-line#452946
    kvm - Can I pass through a USB Port via qemu Command Line? - Unix & Linux Stack Exchange

  * https://unix.stackexchange.com/questions/310271/camera-passthrough-in-qemu-kvm-linux-guest-machine
    Camera passthrough in QEMU/KVM (linux) guest machine - Unix & Linux Stack Exchange

  * https://bugs.launchpad.net/qemu/+bug/1377163/
    Bug #1377163 “Does not add usb-host devices as they are hotplugg...” : Bugs : QEMU

  * https://bugzilla.redhat.com/show_bug.cgi?id=955438
    955438 – qemu should fail to boot guest when speed mismatch trying to attach low speed usb device to xHCI high speed controller

  * https://bugs.launchpad.net/qemu/+bug/1550743
    Bug #1550743 “connect low speed host devices to qemu ehci does n...” : Bugs : QEMU

  * https://bugzilla.redhat.com/show_bug.cgi?id=949505
    949505 – Assign 3.0 usb device to ehci and uhci or usb hub got applied anyway with warning but fails to function

  * https://www.reddit.com/r/linuxadmin/comments/7a7bgl/when_adding_usb_redirection_in_a_vm_it_resets_the/
    When adding USB redirection in a vm it resets the USB device and errors with speed mismatch. Any idea how to change speed? : linuxadmin

  * https://bugs.launchpad.net/qemu/+bug/1810000
    qemu system emulator crashed when using xhci usb controller

## Manually resizing an image

Nowadays this can be done with `kvmx growpart <device> <partition> <additional_size>`, but
here goes some manual procedures if needed.

Image resize can be manually done with a procedure like this thanks to [these docs][]:

    # poweroff
    kvmx poweroff $guest

    # resize image
    qemu-img resize `kvmx list_image $guest` +5G

    # power up
    kvmx up $guest

    # ensure parted is installed
    #sudo apt-get install -y parted
    kvmx ssh $guest sudo apt-get install -y cloud-guest-utils

    # resize virtual machine root fs - while the partition is mounted!
    # this parted command currently need to be done manually
    #echo resizepart 2 -1 | kvmx ssh $guest sudo parted /dev/vda

    # See https://unix.stackexchange.com/questions/373063/auto-expand-last-partition-to-use-all-unallocated-space-using-parted-in-batch-m
    #     https://unix.stackexchange.com/questions/190317/gnu-parted-resizepart-in-script#202872
    #     https://bugs.launchpad.net/ubuntu/+source/parted/+bug/1270203
    #     https://techtitbits.com/2018/12/using-parteds-resizepart-non-interactively-on-a-busy-partition/
    #     https://serverfault.com/questions/870594/resize-partition-to-maximum-using-parted-in-non-interactive-mode
    #kvmx ssh $guest sudo parted /dev/vda resizepart 2 -1 Yes
    kvmx ssh $guest sudo growpart /dev/vda 2

    kvmx ssh $guest sudo resize2fs /dev/vda2
    kvmx ssh $guest sudo touch /forcefsck
    kvmx restart $guest

[these docs]: https://ahelpme.com/linux/online-resize-of-a-root-ext4-file-system-increase-the-space/

## Folder sharing

### Virtio

References on virtio:

* [Virtio on Linux — The Linux Kernel documentation](https://www.kernel.org/doc/html/v6.8/driver-api/virtio/virtio.html)
  * [linux kernel - VIRTIO: How it increase performance - Stack Overflow](https://stackoverflow.com/questions/24737882/virtio-how-it-increase-performance)
      * [Virtual I/O Device (VIRTIO) Version 1.1](https://docs.oasis-open.org/virtio/virtio/v1.1/cs01/virtio-v1.1-cs01.html#x1-240006)

### With virtio and 9p

* Status: works on kvmx.
* Limitations: performance is low on Linux (as of 2024-08-01), due to a
  limitation in the kernel.
* Since fixes for CVE-2023-2861 were released, it's not possible anymore to set
  UNIX sockets in shared folders.

#### Details

Some references on 9p folder sharing:

* [v9fs: Plan 9 Resource Sharing for Linux — The Linux Kernel documentation](https://www.kernel.org/doc/html/latest/filesystems/9p.html)
  * [Documentation/9p - QEMU](https://wiki.qemu.org/Documentation/9p)
  * [Documentation/9psetup - QEMU](https://wiki.qemu.org/Documentation/9psetup)
  * [v9fs · GitHub](https://github.com/v9fs)

Support for opening sockets was removed with fixes for CVE-2023-2861:

* [Os boot issues on 9p filesystem due to unix domain sockets open failure (#2337) · Issues · QEMU / QEMU · GitLab](https://gitlab.com/qemu-project/qemu/-/issues/2337)
* [9pfs: prevent opening special files (CVE-2023-2861) (f6b0de53) · Commits · QEMU / QEMU · GitLab](https://gitlab.com/qemu-project/qemu/-/commit/f6b0de53fb87ddefed348a39284c8e2f28dc4eda)
* [CVE - CVE-2023-2861](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2023-2861)
* [SECURITY DLA 3759-1 qemu security update](https://lists.debian.org/debian-lts-announce/2024/03/msg00012.html)

#### Performance

Performance limitations:

* There's a limit in the Linux kernel, which is explained in [this
  message](https://lists.gnu.org/archive/html/qemu-devel/2021-02/msg06850.html)
  as being `.maxsize = PAGE_SIZE * (VIRTQUEUE_NUM - 3)`.
* The limit is present at `trans_virtio.c` (as of 2024-08-01):
  [linux/net/9p/trans_virtio.c at master - L803 · torvalds/linux ·
  GitHub](https://github.com/torvalds/linux/blob/master/net/9p/trans_virtio.c#L803)
* [History for net/9p/trans_virtio.c - torvalds/linux · GitHub](https://github.com/torvalds/linux/commits/master/net/9p/trans_virtio.c)

Discussion an proposals to overcome this limit:

* [Can not set high msize with virtio-9p (Was: Re: virtiofs vs 9p performan](https://lists.gnu.org/archive/html/qemu-devel/2021-02/msg06343.html)
  * [Re: Can not set high msize with virtio-9p (Was: Re: virtiofs vs 9p perfo](https://lists.gnu.org/archive/html/qemu-devel/2021-02/msg06850.html)
* [remove msize limit in virtio transport - LWN.net](https://lwn.net/Articles/901523/)
  * [New Patches Aim To Boost Linux 9p Performance By ~10x - Phoronix](https://www.phoronix.com/news/Linux-9p-10x-Performance)
    * [9p performance increase by ~10x reflected in WSL? · microsoft/WSL · Discussion #9412 · GitHub](https://github.com/microsoft/WSL/discussions/9412)
    * [VM: 9p: degraded performance: a reasonable high msize should be chosen on client/guest side · Issue #10219 · canonical/lxd · GitHub](https://github.com/canonical/lxd/issues/10219)
      * [Add msize configuration for windows-side 9P client (=higher WSL2 disk speed) · Issue #9125 · microsoft/WSL · GitHub](https://github.com/microsoft/WSL/issues/9125)
  * [[Virtio-fs] Can not set high msize with virtio-9p (Was: Re: virtiofs vs 9p performance)](https://listman.redhat.com/archives/virtio-fs/2021-March/002811.html)

Patches:

* [[PATCH v6 00/11] remove msize limit in virtio transport [LWN.net]](https://lwn.net/ml/linux-kernel/cover.1657920926.git.linux_oss@crudebyte.com/)
* [[PATCH v5 00/11] remove msize limit in virtio transport - Christian Schoenebeck](https://lore.kernel.org/all/cover.1657636554.git.linux_oss@crudebyte.com/)
* [Performance fixes for 9p filesystem - LWN.net](https://lwn.net/Articles/918213/)
* Other (untriaged list):
  * [Re: [PATCH v5 00/11] remove msize limit in virtio transport - Christian Schoenebeck](https://lore.kernel.org/all/7468612.NupLhYsxyy@silver/)
  * [[PATCH v3 0/7] net/9p: remove msize limit in virtio transport - Christian Schoenebeck](https://lore.kernel.org/netdev/cover.1632327421.git.linux_oss@crudebyte.com/)
  * [[PATCH v2 1/7] net/9p: show error message if user 'msize' cannot be satisfied — Netdev](https://www.spinics.net/lists/netdev/msg768036.html)
  * [[PATCH v2 00/10] Performance fixes for 9p filesystem - Eric Van Hensbergen](https://lore.kernel.org/lkml/20221218232217.1713283-1-evanhensbergen@icloud.com/)
  * [[Virtio-fs] [PATCH v2 0/3] virtio: increase VIRTQUEUE_MAX_SIZE to 32k](https://listman.redhat.com/archives/virtio-fs/2021-October/004135.html)
  * [[v4,08/12] net/9p: limit 'msize' to KMALLOC_MAX_SIZE for all transports - Patchwork](https://patchwork.kernel.org/project/netdevbpf/patch/39f81db5e5b25a1e4f94ad3b05552044209aff21.1640870037.git.linux_oss@crudebyte.com/)
  * [Thread: [V9fs-developer] [PATCH] Improve 9p performance for read operations | Plan 9 Resource Sharing for Linux](https://sourceforge.net/p/v9fs/mailman/v9fs-developer/thread/B272E6A0-C349-4B23-BE6F-7CBA8D6C4B6B%40icloud.com/)

### With virtiofs

* The `virtiofsd` implementation from upstream QUEMY was removed on [version
  8.0](https://wiki.qemu.org/ChangeLog/8.0), in favor of the [virtio-fs /
  virtiofsd](https://gitlab.com/virtio-fs/virtiofsd) implementation.
* [virtiofs - shared file system for virtual machines](https://virtio-fs.gitlab.io/)
  * [virtiofs - shared file system for virtual machines / Standalone usage](https://virtio-fs.gitlab.io/howto-qemu.html)
    * [Debian -- Details of package virtiofsd in trixie](https://packages.debian.org/trixie/virtiofsd)
      * [rust-virtiofsd - Debian Package Tracker](https://tracker.debian.org/pkg/rust-virtiofsd)
  * [libvirt: Sharing files with Virtiofs](https://libvirt.org/kbase/virtiofs.html)
    * [virtio-fs / virtiofsd · GitLab](https://gitlab.com/virtio-fs/virtiofsd)
    * [VirtIO-FS Is Looking Quite Good For Shared File-System With VMs - Phoronix](https://www.phoronix.com/news/VirtIO-FS-Looking-Good-2020)
