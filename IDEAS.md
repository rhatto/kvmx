# Ideas

## Workflow

### Continuous Integration script (kvmx-ci)

* For schedule jobs (like cron).
* Pull from remote repository and check source integrity.
* Automatic VM (re-)creation and/or provisioning.
* Logging.
* Status report.

### Test suite

* Test suite for kvmx itself.

### Provisioning improvements

* Option `provision_always` to provision whenever a guest is started.

* Option to re-create the backing file whenever a guest is started (if backing
  file is enabled).

### Qubes-like behavior

* A qubes-like behavior: guest configured:
  * Option 1:
    * With backing file and a basebox as a TemplateVM, using a `template_vm`
      config working along with `base` abd `backing_file`.
    * To reinitialize de backing file at every restart.
    * Guest won't start if the basebox is running.
  * Option 2:
    * Starting the VM using -snapshot param.
    * Setting the $image to the same as the basebox.
  * Use `$shared_folders` to mount custom data into the guest, allowing for
    specific /home/user contents.

### Lifecycle enhancements

  * kvmx recycle <vm-name> # kvmx stop/destroy/up/provision
  * kvmx mount   <vm-name> <host-folder> <guest-mountpoint> # live 9p mounting
  * kvmx run     <vm-name> <command-file> # uploads a script as a temp file in the guest and runs it
  * kvmx upgrade <vm-name> # upgrades via SSH using a pre-defined upgrade command in the kvmxfile,
                           # start the VM if needed, skip if no command is set (skip when backing_file=1?);
                           # that could allow for mass upgrades using `kvmx-supervise foreach upgrade`

### Disposability enhancements

* KVMX disposable enhancements:
  * kvmx disposable      <vm-name> [command]
  * kvmx disposable-run  <vm-name> <command-file>
  * kvmx disposable-name <vm-name> <temporary-name>
  * kvmx mat             <vm-name> <filename>
  * kvmx pdf-cleaner     <vm-name> <filename>
  * kvmx clamav          <vm-name> <folder|file>

## Folder sharing

* Alternative folder sharing support:
  * NFS, SMB.
  * Or even [SSH to a server](https://superuser.com/questions/831659/mount-a-local-directory-to-a-remote-ssh-server)).

## Image handling

* Support for other OSes like Arch Linux, Gentoo, Alpine, CentOS and NixOS.

* Integration with [image-bootstrap](https://github.com/hartwork/image-bootstrap).

* LVM support at `kvmx-create`, with optional LUKS support for the whole volume.

* Non-superuser support for building images on `kvmx-create`, relying on a method like the following:
  * https://unix.stackexchange.com/questions/32008/how-to-mount-an-image-file-without-root-permission
  * https://wiki.debian.org/ManipulatingISOs
  * https://github.com/libfuse/libfuse/wiki/Filesystems
  * https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=all&keywords=fuse

* Additional way to handle missing image on `kvmx up` by spawning `kvmx_install` instead of `kvmx-create`.
