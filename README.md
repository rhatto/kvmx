# KVMX: vagrant-like QEMU KVM Wrapper

KVMX is a lightweight implementation of a virtual machine manager
inspired by [vagrant](https://www.vagrantup.com/),
[kvm-manager](https://git.fluxo.info/kvm-manager/about/) and
[bocker](https://github.com/p8952/bocker).

It may be used for development, as a wrapper for desktop GUI isolation or
even in small production environments.

This is simple stuff. Don't use it if you need any complex behavior
or integration. In the other hand, if you're looking for a small
application that doesn't depend on software installed from unstrusted
sources, you'll feel welcome here :)

## Features

* Automatic provisioning for Debian-based guests.
* Serial console support.
* QEMU monitor support.
* Easily copy files or SSH into the guest.
* Built-in shell.
* GUI support via Spice, Xephyr, VNC and Xpra.
* Safer clipboard sharing through the guest's SSH server.
* Works both for desktop GUI virtualization, local development or at production servers.

## Dependencies

KVMX currently needs a Debian-based system and the following packages depending on what
features you're interested to run:

    sudo apt install git qemu qemu-kvm virt-viewer spice-client-gtk socat screen sshfs usbutils

If you plan to create guest images, you may also want the following packages:

    sudo apt install debootstrap grub-pc parted mbr libguestfs-tools

If you want Xpra and Xephyr support:

    sudo apt install xpra xserver-xephyr

## Instalation

Simply clone it and add to your `$PATH`:

    git clone https://git.fluxo.info/kvmx

You can also verify the latest commit's OpenPGP signature:

    /usr/bin/git -C kvmx verify-commit HEAD

Note that `/usr/bin/git` is called to avoid any other `git` wrappers or aliases
you might have available on your shell.

## Basic usage

    kvmx init [project-name] [project-folder] # initialize
    kvmx edit [project-name]                  # optional customization
    kvmx up   [project-name]                  # bring it up!

If no project name is specified, the current folder name is assumed as the project name.
If no folder is specified, the current folder is assumed as the project home.

## Using with existing guests

Instead of a simple `kvmx up`, you might want to use an existing virtual machine.
Simply point the `image` parameter at your project's `kvmxfile` to where your image
resides.

## Manually creating a guest

Alternativelly, you might create a new one by hand. To do so, proceed as usual
with `kvmx init` and `kvmx edit` acording to the Basic Usage stated above and
then type

    kvmx install <project-name> ~/path/to/install.iso

This will boot and installation media with you guest's disk available for
regular system install.

If you want OpenSSH functionality, make sure to create an user and set a password
related to the configuration present at the project's `kvmxfile`.

Also, make sure to to create an OpenSSH keypair for this virtual machine and
put the public key into the guest user's home folder. That can be done simply
by cloning `kvmx` repo inside the guest and installing the provided insecure
key into place (once the machine is fully acessible via `kvmx ssh` you can
rotate the keys with the `rotate_sshkeys` action).

Passwordless sudo might also be desired for the full development functionality.

You can test your new system with

    kvmx install <project-name> /dev/null

Then, if everything is fine, stop the guest and turn it on again, this time with

    kvmx up <project-name>

## Clipboard

Cliboard sharing is available by using a SSH server inside the guest:

* Use `kvmx-clipboard` to send and receive content to and from guests.
* `kvmx-spice-copy` and `kvmx-spice-paste` are handy wrappers to integrate with
  your desktop environment.

## Other features

Check the other available commands with:

    kvmx help

## References

* The research which resulted in `kvmx` began in the [Virtualized GUI
  environments](https://blog.fluxo.info/suckless/virtual) article.
