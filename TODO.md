# TODO

## Fixes

* [ ] Issue with SSH access on `trixie` VMs: maybe the default ciphers have changed?

* [ ] Support for `virt-viewer` is currently broken (as of 2024-08-04).

* [ ] Improve `xrandr` handling (not working on big monitors with very high
      resolutions).

## Usability

* [ ] Add a `cryptdisks` action to handle unlocking of encrypted volumes used
      by a guest VM. The specific unlocking procedure should be configurable,
      and the action must test whether the volume is available and not already
      unlocked.

* [ ] Hard pause VM (--hard): besides pausing the process, also try to pause it
      in the QEMU monitor.

* [ ] Submit patch for spice-client-gtk for menuless windows (spice usecase) to
      Debian.

* [ ] Docs (tutorial and manpage).

* [ ] Makefile and debian package.

* [ ] Systemd service for a single VM.

* [ ] Shell completions.

* [ ] Support for other Spice clients such as [Remmina](https://remmina.org).

* [ ] Support for user-wide configurations that override both the sample and the
      guest `kvmxfile`.

* [ ] Support for multiple source/targets pairs (analogous to `shared_folders`) at:
  * [ ] `provision_rsync`.

* [ ] Be more distro-agnostic, making any debian-specific routine as a separate function.

* [ ] Be more agnostic on some personal choices (locales, softwares etc).

* [ ] Support for per-guest `known_hosts` for SSH logins.

* [ ] Support for storing guest VMs in (remote) repositories, with supporing
      actions such as `repo`, `push`, `pull` etc.

## Virtualization

* [ ] Config option to [disable
      networking](https://wiki.qemu.org/Documentation/Networking#How_to_disable_network_completely),
      passing `-net none`.

* [ ] [Nested virtualization](http://www.rdoxenham.com/?p=275)
      ([1](https://wiki.archlinux.org/index.php/KVM#Nested_virtualization),
      [2](https://ladipro.wordpress.com/2017/02/24/running-hyperv-in-kvm-guest/)).

## Folder sharing

* [ ] Dynamically add PCI bridges depending on the number of shared folders,
      avoiding PCI slot exhaustion.

* [ ] Mount/umount/remount commands to manage shared folders.

* [ ] Try to umount all sshfs volumes in the host when powering off.

* [ ] Remount 9p shared folders and reinitialize spice-vdagent upon resume from
      disk [see possible bug](https://bugzilla.redhat.com/show_bug.cgi?id=1333072).

* [ ] Support for [virtiofs](https://virtio-fs.gitlab.io/howto-qemu.html).

## Audio fixes to avoid crackling on input

* [ ] Implement an option to reduce crackling on sound input.

The fix involves setting the following variables:

    export QEMU_AUDIO_DRV="pa"
    export QEMU_PA_SAMPLES="8192"
    export QEMU_AUDIO_TIMER_PERIOD="99"

References:

* https://stackoverflow.com/questions/32193050/qemu-pulseaudio-and-bad-quality-of-sound#35998501
* https://www.reddit.com/r/VFIO/comments/542bw1/ha_got_rid_of_the_pulse_audio_crackling/
* https://www.reddit.com/r/VFIO/comments/ibmjs3/finally_fixed_crackling_audio_when_passing/
* https://www.reddit.com/r/VFIO/comments/746t4h/getting_rid_of_audio_crackling_once_and_for_all/
* https://www.reddit.com/r/VFIO/comments/8aqju8/audio_crackling_when_routing_output_to_pulseaudio/

## Simultaneous clients connections

* [ ] Implement the following:

    export SPICE_DEBUG_ALLOW_MC=1

For explanation, check https://www.spice-space.org/multiple-clients.html

## Image handling

* [ ] Fix isolinux support:
  * http://www.syslinux.org/wiki/index.php?title=Development/Testing
  * http://linux-kernel-driver.blogspot.com.br/2009/06/linux-kernel-development-using.html
  * https://bbs.archlinux.org/viewtopic.php?id=177299
