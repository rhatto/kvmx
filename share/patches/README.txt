# Support for toggling Menubar visibility on spicy

This patch adds a submenu item at the Views menu item which allows to hide and
show the Menubar.

The functionality is also available using Shift+F6 keyboard combination.

This feature makes possible a more seamless integration between spice guests
and the host's window manager.

The patch was rejected by upstream, so it now lives at KVMX tree.

See https://lists.freedesktop.org/archives/spice-devel/2018-November/046208.html
